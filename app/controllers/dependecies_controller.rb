class DependeciesController < ApplicationController
  before_action :set_dependecy, only: [:show, :edit, :update, :destroy]

  # GET /dependecies
  # GET /dependecies.json
  def index
    @dependecies = Dependecy.all
  end

  # GET /dependecies/1
  # GET /dependecies/1.json
  def show
  end

  # GET /dependecies/new
  def new
    @dependecy = Dependecy.new
  end

  # GET /dependecies/1/edit
  def edit
  end

  # POST /dependecies
  # POST /dependecies.json
  def create
    @dependecy = Dependecy.new(dependecy_params)

    respond_to do |format|
      if @dependecy.save
        format.html { redirect_to @dependecy, notice: 'Dependecy was successfully created.' }
        format.json { render :show, status: :created, location: @dependecy }
      else
        format.html { render :new }
        format.json { render json: @dependecy.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dependecies/1
  # PATCH/PUT /dependecies/1.json
  def update
    respond_to do |format|
      if @dependecy.update(dependecy_params)
        format.html { redirect_to @dependecy, notice: 'Dependecy was successfully updated.' }
        format.json { render :show, status: :ok, location: @dependecy }
      else
        format.html { render :edit }
        format.json { render json: @dependecy.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dependecies/1
  # DELETE /dependecies/1.json
  def destroy
    @dependecy.destroy
    respond_to do |format|
      format.html { redirect_to dependecies_url, notice: 'Dependecy was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_dependecy
      @dependecy = Dependecy.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def dependecy_params
      params.require(:dependecy).permit(:nombre, :descripcion)
    end
end
