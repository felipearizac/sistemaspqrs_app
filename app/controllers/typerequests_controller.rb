class TyperequestsController < ApplicationController
  before_action :set_typerequest, only: [:show, :edit, :update, :destroy]

  # GET /typerequests
  # GET /typerequests.json
  def index
    @typerequests = Typerequest.all
  end

  # GET /typerequests/1
  # GET /typerequests/1.json
  def show
  end

  # GET /typerequests/new
  def new
    @typerequest = Typerequest.new
  end

  # GET /typerequests/1/edit
  def edit
  end

  # POST /typerequests
  # POST /typerequests.json
  def create
    @typerequest = Typerequest.new(typerequest_params)

    respond_to do |format|
      if @typerequest.save
        format.html { redirect_to @typerequest, notice: 'Typerequest was successfully created.' }
        format.json { render :show, status: :created, location: @typerequest }
      else
        format.html { render :new }
        format.json { render json: @typerequest.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /typerequests/1
  # PATCH/PUT /typerequests/1.json
  def update
    respond_to do |format|
      if @typerequest.update(typerequest_params)
        format.html { redirect_to @typerequest, notice: 'Typerequest was successfully updated.' }
        format.json { render :show, status: :ok, location: @typerequest }
      else
        format.html { render :edit }
        format.json { render json: @typerequest.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /typerequests/1
  # DELETE /typerequests/1.json
  def destroy
    @typerequest.destroy
    respond_to do |format|
      format.html { redirect_to typerequests_url, notice: 'Typerequest was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_typerequest
      @typerequest = Typerequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def typerequest_params
      params.require(:typerequest).permit(:nombre, :descripcion)
    end
end
