class PermissionRole < ApplicationRecord
  belongs_to :permissions
  belongs_to :role
  audited
end
