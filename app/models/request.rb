class Request < ApplicationRecord
  belongs_to :typerequest
  belongs_to :state
  audited
  attribute :state_id, :integer, default: '1'

  has_attached_file :cover
  validates_attachment_content_type :cover, content_type: [
	"application/pdf", "application/msword", 
	"application/vnd.openxmlformats-officedocument.wordprocessingml.document", 
	"application/vnd.oasis.opendocument.text",
	"application/vnd.ms-excel",
	"application/vnd.ms-office",
	"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
	"application/vnd.oasis.opendocument.spreadsheet"
   ]
end
