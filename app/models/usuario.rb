class Usuario < ApplicationRecord
  belongs_to :user
  belongs_to :role
  audited
end
