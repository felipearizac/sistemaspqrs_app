json.extract! permission_role, :id, :permissions_id, :role_id, :created_at, :updated_at
json.url permission_role_url(permission_role, format: :json)
