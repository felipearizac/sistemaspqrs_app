json.extract! permission, :id, :tipo_permiso, :descripcion, :created_at, :updated_at
json.url permission_url(permission, format: :json)
