json.extract! request, :id, :nombre_solicitante, :apellido_solicitante, :email, :telefono, :direccion_solicitante, :typerequest_id, :detalles, :document_id, :state_id, :created_at, :updated_at
json.url request_url(request, format: :json)
