class AddFileToCreateRequests < ActiveRecord::Migration[5.2]
  def change
    add_column :create_requests, :file, :string
  end
end
