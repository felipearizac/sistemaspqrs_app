Rails.application.routes.draw do
  	
  resources :dependecies
  resources :permission_roles
  resources :requests
  resources :states
  resources :typerequests
  resources :usuarios
  devise_for :users
  resources :permissions
  resources :roles
  	root 'welcome#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
