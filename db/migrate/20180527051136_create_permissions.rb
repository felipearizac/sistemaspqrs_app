class CreatePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :permissions do |t|
      t.string :tipo_permiso
      t.string :descripcion

      t.timestamps
    end
  end
end
 