class CreateRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :requests do |t|
      t.string :nombre_solicitante
      t.string :apellido_solicitante
      t.string :email
      t.integer :telefono
      t.string :direccion_solicitante
      t.references :typerequest, foreign_key: true
      t.text :detalles
      t.references :state, foreign_key: true

      t.timestamps
    end
  end
end
