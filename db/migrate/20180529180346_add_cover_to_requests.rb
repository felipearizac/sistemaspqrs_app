class AddCoverToRequests < ActiveRecord::Migration[5.2]
  def change
  	add_attachment :requests, :cover
  end
end
