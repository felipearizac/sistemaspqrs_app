class AddDependencieToRoles < ActiveRecord::Migration[5.2]
  def change
    add_reference :roles, :dependencie, foreign_key: true
  end
end
