require 'test_helper'

class DependeciesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dependecy = dependecies(:one)
  end

  test "should get index" do
    get dependecies_url
    assert_response :success
  end

  test "should get new" do
    get new_dependecy_url
    assert_response :success
  end

  test "should create dependecy" do
    assert_difference('Dependecy.count') do
      post dependecies_url, params: { dependecy: { descripcion: @dependecy.descripcion, nombre: @dependecy.nombre } }
    end

    assert_redirected_to dependecy_url(Dependecy.last)
  end

  test "should show dependecy" do
    get dependecy_url(@dependecy)
    assert_response :success
  end

  test "should get edit" do
    get edit_dependecy_url(@dependecy)
    assert_response :success
  end

  test "should update dependecy" do
    patch dependecy_url(@dependecy), params: { dependecy: { descripcion: @dependecy.descripcion, nombre: @dependecy.nombre } }
    assert_redirected_to dependecy_url(@dependecy)
  end

  test "should destroy dependecy" do
    assert_difference('Dependecy.count', -1) do
      delete dependecy_url(@dependecy)
    end

    assert_redirected_to dependecies_url
  end
end
