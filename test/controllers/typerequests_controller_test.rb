require 'test_helper'

class TyperequestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @typerequest = typerequests(:one)
  end

  test "should get index" do
    get typerequests_url
    assert_response :success
  end

  test "should get new" do
    get new_typerequest_url
    assert_response :success
  end

  test "should create typerequest" do
    assert_difference('Typerequest.count') do
      post typerequests_url, params: { typerequest: { descripcion: @typerequest.descripcion, nombre: @typerequest.nombre } }
    end

    assert_redirected_to typerequest_url(Typerequest.last)
  end

  test "should show typerequest" do
    get typerequest_url(@typerequest)
    assert_response :success
  end

  test "should get edit" do
    get edit_typerequest_url(@typerequest)
    assert_response :success
  end

  test "should update typerequest" do
    patch typerequest_url(@typerequest), params: { typerequest: { descripcion: @typerequest.descripcion, nombre: @typerequest.nombre } }
    assert_redirected_to typerequest_url(@typerequest)
  end

  test "should destroy typerequest" do
    assert_difference('Typerequest.count', -1) do
      delete typerequest_url(@typerequest)
    end

    assert_redirected_to typerequests_url
  end
end
