require "application_system_test_case"

class DependeciesTest < ApplicationSystemTestCase
  setup do
    @dependecy = dependecies(:one)
  end

  test "visiting the index" do
    visit dependecies_url
    assert_selector "h1", text: "Dependecies"
  end

  test "creating a Dependecy" do
    visit dependecies_url
    click_on "New Dependecy"

    fill_in "Descripcion", with: @dependecy.descripcion
    fill_in "Nombre", with: @dependecy.nombre
    click_on "Create Dependecy"

    assert_text "Dependecy was successfully created"
    click_on "Back"
  end

  test "updating a Dependecy" do
    visit dependecies_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @dependecy.descripcion
    fill_in "Nombre", with: @dependecy.nombre
    click_on "Update Dependecy"

    assert_text "Dependecy was successfully updated"
    click_on "Back"
  end

  test "destroying a Dependecy" do
    visit dependecies_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dependecy was successfully destroyed"
  end
end
