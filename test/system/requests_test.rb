require "application_system_test_case"

class RequestsTest < ApplicationSystemTestCase
  setup do
    @request = requests(:one)
  end

  test "visiting the index" do
    visit requests_url
    assert_selector "h1", text: "Requests"
  end

  test "creating a Request" do
    visit requests_url
    click_on "New Request"

    fill_in "Apellido Solicitante", with: @request.apellido_solicitante
    fill_in "Detalles", with: @request.detalles
    fill_in "Direccion Solicitante", with: @request.direccion_solicitante
    fill_in "Document", with: @request.document_id
    fill_in "Email", with: @request.email
    fill_in "Nombre Solicitante", with: @request.nombre_solicitante
    fill_in "State", with: @request.state_id
    fill_in "Telefono", with: @request.telefono
    fill_in "Typerequest", with: @request.typerequest_id
    click_on "Create Request"

    assert_text "Request was successfully created"
    click_on "Back"
  end

  test "updating a Request" do
    visit requests_url
    click_on "Edit", match: :first

    fill_in "Apellido Solicitante", with: @request.apellido_solicitante
    fill_in "Detalles", with: @request.detalles
    fill_in "Direccion Solicitante", with: @request.direccion_solicitante
    fill_in "Document", with: @request.document_id
    fill_in "Email", with: @request.email
    fill_in "Nombre Solicitante", with: @request.nombre_solicitante
    fill_in "State", with: @request.state_id
    fill_in "Telefono", with: @request.telefono
    fill_in "Typerequest", with: @request.typerequest_id
    click_on "Update Request"

    assert_text "Request was successfully updated"
    click_on "Back"
  end

  test "destroying a Request" do
    visit requests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Request was successfully destroyed"
  end
end
