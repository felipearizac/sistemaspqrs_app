require "application_system_test_case"

class TyperequestsTest < ApplicationSystemTestCase
  setup do
    @typerequest = typerequests(:one)
  end

  test "visiting the index" do
    visit typerequests_url
    assert_selector "h1", text: "Typerequests"
  end

  test "creating a Typerequest" do
    visit typerequests_url
    click_on "New Typerequest"

    fill_in "Descripcion", with: @typerequest.descripcion
    fill_in "Nombre", with: @typerequest.nombre
    click_on "Create Typerequest"

    assert_text "Typerequest was successfully created"
    click_on "Back"
  end

  test "updating a Typerequest" do
    visit typerequests_url
    click_on "Edit", match: :first

    fill_in "Descripcion", with: @typerequest.descripcion
    fill_in "Nombre", with: @typerequest.nombre
    click_on "Update Typerequest"

    assert_text "Typerequest was successfully updated"
    click_on "Back"
  end

  test "destroying a Typerequest" do
    visit typerequests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Typerequest was successfully destroyed"
  end
end
